import firebase from "firebase";

import "firebase/auth";

const firebaseConfig = {
    apiKey: "AIzaSyCJQCXZK3y3rWFE8lJvagOAXTE7gQX8MFI",
    authDomain: "vapestore-999.firebaseapp.com",
    projectId: "vapestore-999",
    storageBucket: "vapestore-999.appspot.com",
    messagingSenderId: "309033100892",
    appId: "1:309033100892:web:29e3fd0cc89eef944ba899",
    measurementId: "G-M2G5JWX5K7"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const googleProvider = new firebase.auth.GoogleAuthProvider();